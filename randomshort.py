import webapp

user = "localhost"
port = 1234

class ShortUrlApp(webapp.webApp):
    url_dict1 = {}
    url_dict2 = {}

    NotFound = ["404 Not Found",
                "<html><body><h1>404 Not Found</h1></body></html>"]

    def fullurl(self, url):

        if "http://" in url or "https://" in url:
            return url
        else:
            return ("http://" + url)

    def url_list(self):
        new_list = ""
        for lista, url in self.url_dict2.items():
            line = ("URL introducida: <a href=" + url + ">" + url +
                    "</a> => URL recortada: <a href=" +
                    lista + ">" + lista + "</a></br>")
            new_list += line
        return new_list

    def parse(self, request):
        request = str(request, 'utf8')
        input = request.split()[0:2]
        if "URL=" in request:
            input.append(request.split('=')[-1])
        print("input: ", input)
        return input

    def process(self, parsedRequest):
        if parsedRequest[0] == "GET":
            if parsedRequest[1] == "/":
                new_list = self.url_list()
                print("mirrorslist: ", new_list)
                msg_200_ok = ("<html><head><meta charset='utf-8'><h1 align='center'>"
                      "MINI PRÁCTICA 1: Mini-1-Acortadora</h1></head><body>"
                      "<form method='POST'><p align='center'>Introduce una URL para acortar: "
                      "<input type='text' name='URL' ></p></form>"
                      "URLs ya registradas:<br>" + new_list + "</body></html>")
                return ("200 OK", msg_200_ok)
            elif parsedRequest[1] in self.url_dict2.keys():
                redirected_url = self.fullurl(self.url_dict2[parsedRequest[1]])
                print(redirected_url)
                msg_url_redirected = ("<html><body>"
                            "<meta http-equiv='refresh' content=0;url=" +
                            redirected_url + "></p></body></html>")
                print(msg_url_redirected)
                return ("300 Redirect", msg_url_redirected)
            else:
                return (self.NotFound)
        elif parsedRequest[0] == "POST":
            if parsedRequest[2] == "":
                return (self.NotFound)
            url = self.fullurl(parsedRequest[2])
            if url not in self.url_dict1:
                key = "/" + str(len(self.url_dict2))
                self.url_dict2[key] = url
                self.url_dict1[url] = key
            full_short_url = "http://localhost:1234" + self.url_dict1[url]
            msg_to_redir = ("<html><body>Url introducida: <a href=" + url +
                        ">" + url + "</a><br>Url acortada:  <a href=" +
                        full_short_url + ">" + full_short_url + "</a></br></body></html>")
            return ("200 OK", msg_to_redir)
        else:
            return (self.NotFound)

if __name__ == "__main__":
    testWebApp = ShortUrlApp(user, port)
